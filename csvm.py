import csv
import re
from sys import stdin


def get_user_info():
	args = list(map(str,stdin.readline().split(', ')))
	args = list(map(lambda s: s.strip(), args))
	args = clense_phonenum(args)
	f = open('users.csv', 'a', encoding='utf-8', newline='')
	wr = csv.writer(f)
	wr.writerow(args)
	f.close()

def get_domain():
	f = open('users.csv','r')
	rdr = csv.reader(f)
	for line in rdr:
		username = line[1].split("@")[0]
		print(username)
	f.close()


def clense_phonenum(row):
	phone_num = row[2]
	row[2] = re.sub('[-=.#/?:$}]', '-', phone_num)
	return row

def lastname_stats():

	lastname_dict = dict()
	f = open('users.csv','r')
	rdr = csv.reader(f)
	for line in rdr:
		lastname = line[0].split(' ')[-1]
		if lastname_dict.get(lastname) is None:
			lastname_dict[lastname] = 1
		else:
			lastname_dict[lastname] += 1

	for lastname,count in lastname_dict.items():
		print(lastname, ':', count)
	f.close()
	